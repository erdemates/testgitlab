"searchResponse": [
    {
        "travelOptions": [
             {
                "segments": [
                    {
                        "bookingCode":          "String",
                        "distance":             "km",
                        "departureAirport":     "IATA 3LC",
                        "departureTime":        "Full ISO 8601 With Numeric Timezone",
                        "arrivalAirport":       "IATA 3LC",
                        "arrivalTime":          "Full ISO 8601 With Numeric Timezone",
                        "flightTime":           "minutes",
                        "airlineCode":          "IATA 2LC",
                        "flightNumber":         "string",
                        "equipment":            "IATA 3LC",
                        "baggage": {
                            "checked": {
                                "number":       "integer",
                                "weight":       "kg"
                            },
                            "cabin": {
                                "number":       "integer",
                                "weight":       "kg"
                            }
                        },
                        "nStops":               "integer",
                        "class":                "string"
                    },
                    ...
                ],
                "totalFlightTime":              "minutes",
                "waitTime":                     "minutes",
                "nSeats":                       "integer"

            },
            ...
        ],
        "returnOptions" : [
            {
                "segments": [
                    {
                        "departureAirport":     "IATA 3LC",
                        "departureTimestamp":   "Full ISO 8601 With Numeric Timezone",
                        "arrivalAirport":       "IATA 3LC",
                        "arrivalTimestamp":     "Full ISO 8601 With Numeric Timezone",
                        "flightTime":           "minutes",
                        "airlineCode":          "IATA 2LC",
                        "flightNumber":         "string",
                        "baggage": {
                            "checked": {
                                "number":       "integer",
                                "weight":       "kg"
                            },
                            "cabin": {
                                "number":       "integer",
                                "weight":       "kg"
                            }
                        },
                        "class":                "string"
                    }
                ],
                "totalFlightTime":              "minutes",
                "waitTime":                     "minutes",
            }
        ],
        "price": {
            "totalPrice":               "integer",
            "totalPriceCurrency":       "integer",
            "basePrice":                "integer",
            "basePriceCurrency"         "Currency 3LC",
            "tax":                      "integer",
            "taxCurrency":              "Currency 3LC",
            "refundable":               "boolean"
        },
    },
    ...
]
