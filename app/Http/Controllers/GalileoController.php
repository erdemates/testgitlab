<?php

namespace App\Http\Controllers;

use Log;
use Cache;
use Illuminate\Http\Request;

class GalileoController extends Controller
{

    private const TARGETBRANCH = 'P7002726';
    private const PROVIDER = '1G';

    private const USER = 'Universal API/uAPI1189214086-2752a905';
    private const PASS = 'j3RCWjke8sDD3nbZSnPc2mtZM';

    private const XMLPATH = '../storage/logs/response.xml';
    private const DEFAULT_TTL = 150;
    private const MAX_SEATS = 9;

    private const DATE_RX  = '/^P(\d*)DT(\d*)H(\d*)M(\d*)S$/';
    private const PRICE_RX = '/^([A-Z][A-Z][A-Z])(\d+\.\d\d)$/';

    // Bu olmadı, her soap envelope böyle gelmiyor. Bazen SOAP-ENV geliyor
    // Regex ile yapmak saçma zaten domdocument kasacağız
    private const SOAP_RX = '/^\s*<SOAP:Enveloper\sxmlns:SOAP=/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
    }
    public function bookFlights(Request $request) {

      $departureXmlString = json_decode($request->input('departureXmlString'));
      $returnXmlString = json_decode($request->input('returnXmlString'));

        $requestObject = [
            'targetBranch'  => self::TARGETBRANCH,
            'provider'      => self::PROVIDER,
            'departureXmlString' => $departureXmlString,
            'returnXmlString'   => $returnXmlString,
        ];


        /* TODO Sanitize Inputs */
        /* TODO Adjust Defaults */
        /* TODO Transform Inputs */

        $info_string = "getFlights called with request object:\n";
        $info_string.= var_export($requestObject, true);
        Log::info($info_string);

        // Mesajın başına newline
        // koymazsan boş cevap dönüyor. View'ın içine koymak da
        // işe yaramıyor.
        $message = "\n" . view('galileo.airpricing_xml', $requestObject);

        Log::info($message);
        $response = $this->bookAirRequest( $message );

        if( file_put_contents(self::XMLPATH, $response) === false ) {
          Log::error('Failed to write XML response to file');
        } else {
          Log::info('Wrote XML Response to:'.self::XMLPATH);
        }

        $dom = new \DOMDocument;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($response);
        $dom->formatOutput = true;
        $xpath = new \DOMXPath($dom);


        return htmlspecialchars($response);



      }
    public function getFlightDetails($fareInfoRef, $fareInfoKey) {

        $requestObject = [
            'targetBranch'  => self::TARGETBRANCH,
            'provider'      => self::PROVIDER,
            'fareInfoRef'   => $fareInfoRef,
            'fareInfoKey'   => base64_decode($fareInfoKey)
        ];

        $info_string = "getFlightDetails called with request object:\n";
        $info_string.= var_export($requestObject, true);
        Log::info($info_string);

        $message = "\n" . view('galileo.farerules_xml', $requestObject);

        Log::info($message);
        $response = $this->sendAirRequest( $message );

        return htmlspecialchars($response);

    }

    public function getFlights(Request $request) {

        $requestObject = [
            'targetBranch'  => self::TARGETBRANCH,
            'provider'      => self::PROVIDER,
            'from'          => $request->get('from'),
            'to'            => $request->get('to'),
            'depDate'       => $request->get('depDate'),
            'retDate'       => $request->get('retDate'),
            'nAdults'       => $request->get('nAdults'),
            'nChildren'     => $request->get('nChildren'),
            'nInfants'      => $request->get('nInfants'),
            'currency'      => $request->get('currency'),
            'hasReturn'     => $request->get('retDate') !== null
        ];

        /* TODO Sanitize Inputs */
        /* TODO Adjust Defaults */
        /* TODO Transform Inputs */

        $info_string = "getFlights called with request object:\n";
        $info_string.= var_export($requestObject, true);
        Log::info($info_string);

        // Bunu çözene kadar canım çıktı. Mesajın başına newline
        // koymazsan boş cevap dönüyor. View'ın içine koymak da
        // işe yaramıyor.
        $message = "\n" . view('galileo.lowfare_xml', $requestObject);

        Log::info($message);
        $response = $this->sendAirRequest( $message );

        if( file_put_contents(self::XMLPATH, $response) === false ) {
          Log::error('Failed to write XML response to file');
        } else {
          Log::info('Wrote XML Response to:'.self::XMLPATH);
        }

        $dom = new \DOMDocument;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($response);
        $dom->formatOutput = true;
        $xpath = new \DOMXPath($dom);
        $xpath->registerNamespace("air", "http://www.travelport.com/schema/air_v42_0");

        // $optionNodes = $xpath->query('//*[local-name()=\'Optionslist\']');
        $optionListNodes = $xpath->query('//air:FlightOptionsList');

        $outputList = [];
        foreach($optionListNodes as $optionListNode) {

            $priceNode = $xpath->query('.//ancestor::air:AirPricePoint', $optionListNode);

            $flightOptions = $xpath->query('air:FlightOption', $optionListNode);
            if( $flightOptions->length > 2 ) throw new \Exception('More than 2 "air:FlightOption"s');
            elseif ( $flightOptions->length < 1 ) throw new \Exception('Fewer than 1 "air:FlightOption"s');

            $flights = [];

            // GİDİŞ BACAKLARI
            $flights['travelOptions'] = [];
            $optionList = $xpath->query('air:Option', $flightOptions[0]);
            foreach ($optionList as $option) {
                array_push($flights['travelOptions'], $this->optionDetails($option, $xpath));
            }

            // DÖNÜŞ VARSA DÖNÜŞ BACAKLARI
            if( $flightOptions->length == 2 ) {
                $flights['returnOptions'] = [];
                $optionList = $xpath->query('air:Option', $flightOptions[1]);
                foreach ($optionList as $option) {
                    array_push($flights['returnOptions'], $this->optionDetails($option, $xpath));
                }
            }

            $totalPriceString = $priceNode[0]->getAttribute('TotalPrice');
            preg_match_all(self::PRICE_RX,$totalPriceString,$matches,PREG_SET_ORDER,0);
            $totalPrice = $matches[0][2];
            $totalPriceCurrency = $matches[0][1];

            $basePriceString = $priceNode[0]->getAttribute('BasePrice');
            preg_match_all(self::PRICE_RX,$basePriceString,$matches,PREG_SET_ORDER,0);
            $basePrice = $matches[0][2];
            $basePriceCurrency = $matches[0][1];

            $taxString = $priceNode[0]->getAttribute('Taxes');
            preg_match_all(self::PRICE_RX,$taxString,$matches,PREG_SET_ORDER,0);
            $tax = $matches[0][2];
            $taxCurrency = $matches[0][1];

            $resultItem = [
                'flights' => $flights,
                'price' => [
                    'totalPrice'            => $totalPrice,
                    'totalPriceCurrency'    => $totalPriceCurrency,
                    'basePrice'             => $basePrice,
                    'basePriceCurrency'     => $basePriceCurrency,
                    'tax'                   => $tax,
                    'taxCurrency'           => $taxCurrency
                ]
            ];

            array_push( $outputList, $resultItem);

        }

        // ob_start();
        // var_dump($outputList);
        // $retval = ob_get_contents();
        // ob_clean();
        //
        // return "<pre>$retval</pre>";

        $this->sortFlightResults($outputList);
        return json_encode($outputList);

    }

    private function optionDetails(\DOMElement $flightOption, \DOMXPath $xpath) {

        // Toplam uçuş süresi hesabı
        $travelTime = $flightOption->getAttribute('TravelTime');
        preg_match_all(self::DATE_RX, $travelTime, $matches,PREG_SET_ORDER,0);
        $bookingInfoNodes = $xpath->query('air:BookingInfo', $flightOption);
        // Gün * 1440 + Saat * 60 + Dakika (dakika olarak, saniyeleri atlıyoruz)
        $totalTime =  $matches[0][1] * 1440 + $matches[0][2] * 60 + $matches[0][3];

        $minSeats = self::MAX_SEATS;
        $segments = [];
        $bookingInfo = [
            'provider'      => 'Galileo',
            'segments'      => [],
            'bookingCodes'  => []
        ];
        $route = 0;
        $totalFlightTime = 0;

        foreach($bookingInfoNodes as $bookingInfoNode) {

            $segmentSeats = $bookingInfoNode->getAttribute('BookingCount');
            if( $minSeats > $segmentSeats ) {
                $minSeats = $segmentSeats;
            }

            $segmentRef =  $bookingInfoNode->getAttribute('SegmentRef');
            $segmentNodes = $xpath->query('//air:AirSegment[@Key=\''.$segmentRef.'\']');
            if($segmentNodes->length > 1 ) {
                throw new \Exception('Too many air:AirSegment results');
            }
            elseif($segmentNodes->length < 1 ) {
                throw new \Exception('Too few air:AirSegment results');
            }
            $segmentNode = $segmentNodes[0];

            $fareInfoRef =  $bookingInfoNode->getAttribute('FareInfoRef');
            $fareInfoNodes = $xpath->query('//air:FareInfo[@Key=\''.$fareInfoRef.'\']');
            if($fareInfoNodes->length > 1 ) {
                throw new \Exception('Too many air:FareInfo results');
            }
            elseif($fareInfoNodes->length < 1 ) {
                throw new \Exception('Too few air:FareInfo results');
            }
            $fareInfoNode = $fareInfoNodes[0];

            $segment = [
                'distance'              => (int) $segmentNode->getAttribute('Distance'),
                'originAirport'         => $segmentNode->getAttribute('Origin'),
                'destinationAirport'    => $segmentNode->getAttribute('Destination'),
                'departureTime'         => $segmentNode->getAttribute('DepartureTime'),
                'arrivalTime'           => $segmentNode->getAttribute('ArrivalTime'),
                'flightTime'            => (int) $segmentNode->getAttribute('FlightTime'),
                'flightNumber'          => $segmentNode->getAttribute('FlightNumber'),
                'aircraftType'          => $segmentNode->getAttribute('Equipment'),
                'airlineCode'           => $segmentNode->getAttribute('Carrier'),
                'nStops'                => (int) $segmentNode->getAttribute('NumberOfStops'),
                'bookingCode'           => $bookingInfoNode->getAttribute('BookingCode'),
                'class'                 => $bookingInfoNode->getAttribute('CabinClass')
            ];

            if( $route === 0 ) {
                $route = $segmentNode->getAttribute('Origin');
            }
            $route .= ' > ' . $segmentNode->getAttribute('Destination');

            // Altdaki kapatılmış satır bazen negatif bekleme süresi yaratıyor
            // $totalFlightTime += $segment['flightTime'];
            // Bunun yerine zamanları birbirinden çıkardım.
            $segment['flightTime'] = (strtotime($segment['arrivalTime']) - strtotime($segment['departureTime']))/60;

            $totalFlightTime += $segment['flightTime'];

            $segmentBookInfo = [
                'Key' => $segmentNode->getAttribute('Key'),
                'ProviderCode' => self::PROVIDER,
                'Carrier' => $segmentNode->getAttribute('Carrier'),
                'Group' => $segmentNode->getAttribute('Group'),
                'FlightNumber' => $segmentNode->getAttribute('FlightNumber'),
                'Destination' => $segmentNode->getAttribute('Destination'),
                'Origin' => $segmentNode->getAttribute('Origin'),
                'DepartureTime' => $segmentNode->getAttribute('DepartureTime'),
                'ArrivalTime' => $segmentNode->getAttribute('ArrivalTime')
            ];

            $bookingCodes = [
                'fareBasis' => $fareInfoNode->getAttribute('FareBasis'),
                'segmentRef' => $segmentRef,
                'bookingCode' => $bookingInfoNode->getAttribute('BookingCode'),
            ];

            array_push($segments, $segment);
            array_push($bookingInfo['segments'], $segmentBookInfo);
            array_push($bookingInfo['bookingCodes'], $bookingCodes);

        }

        return [
            'route' => $route,
            'segments' => $segments,
            'totalTime' => $totalTime,
            'totalFlightTime' => $totalFlightTime,
            'waitTime' => $totalTime - $totalFlightTime,
            'nSeats' => $minSeats,
            'fareInfoRef' => $fareInfoRef,
            'bookingInfo' => $bookingInfo
        ];
    }

    private function sortFlightResults( $sortable ) {

        $sortSuccess = uasort($sortable, function($a, $b) {

            if( $a['price']['totalPriceCurrency'] !== $b['price']['totalPriceCurrency'] )
                throw new \Exception('Currency mismatch while sorting flight results');

            $aPrice = (float) $a['price']['totalPrice'];
            $bPrice = (float) $b['price']['totalPrice'];

            if( $aPrice == $bPrice )
                return 0;
            elseif( $aPrice > $bPrice )
                return 1;
            else
                return -1;

        });

        if( !$sortSuccess )
            throw new \Exception('uasort() failed to sort flight results');

    }

    private function checkSearchInputs() {

    }

    private function checkBookingInputs() {

    }

    private function errorCode( $message ) {

        // TODO: Make much better
        /* Error Codes Spec:

            0: No Error
            1: Can Not Connect
            2: Bad response
            3: No Response
            4: No Results
            5: Missing Parameter
            6: Invalid Parameter

        */

        // Eğer sonuç SOAP ile başlıyorsa muhtemelen hatasız sonuç aldık
        if( preg_match_all(self::SOAP_RX, $message, $matches) ) {
            return 0;
        } elseif( strlen($message) == 0 ) {
            return 3;
        } else {
            return 1;
        }

    }

    private function sendAirRequest( $message ) {

        // TODO: Make Sure the response is valid before caching it
        // TODO: Replace default TTL with Response/Provider Specific TTL
        // TODO: Replace Value with Structure to Avoid Hashing collisions

        $messageHash = md5($message);

        if( Cache::has($messageHash) ) {
            Log::info('Cache Hit, Returning saved result');
            return Cache::get($messageHash);
        }

        $soap_do = curl_init('https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService');
        $header = array(
            'Content-Type: text/xml;charset=UTF-8',
            'Accept: gzip,deflate',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
            'SOAPAction: ""',
            'Authorization: Basic '. base64_encode(self::USER . ':' . self::PASS),
            'Content-length: ' . strlen($message),
        );
        $hstring = implode("\r\n", $header);
        Log::info("Soap Header: \r\n$hstring");

        // curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 60);
        // curl_setopt($soap_do, CURLOPT_TIMEOUT, 60);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $message);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);

        Log::info("Sending Soap: $soap_do");
        $response = curl_exec($soap_do);
        if($response === false) {
          Log::error('Curl Failed With Errror: ' . curl_error($soap_do));
        } else {
          Log::info('Got a response with length: ' . strlen($response));
        }

        // TODO: Make Sure the response is valid before caching it
        // TODO: Replace default TTL with Response/Provider Specific TTL
        // TODO: Replace Value with Structure to Avoid Hashing collisions
        $errorCode = $this->errorCode( $response );
        if( $errorCode === 0 ) {
            Cache::put($messageHash, $response, self::DEFAULT_TTL);
        }
        return $response;

    }
    private function bookAirRequest( $message ) {

        // TODO: Make Sure the response is valid before caching it
        // TODO: Replace default TTL with Response/Provider Specific TTL
        // TODO: Replace Value with Structure to Avoid Hashing collisions

        $messageHash = md5($message);

        if( Cache::has($messageHash) ) {
            Log::info('Cache Hit, Returning saved result');
            return Cache::get($messageHash);
        }

        $soap_do = curl_init('https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService');
        $header = array(
            'Content-Type: text/xml;charset=UTF-8',
            'Accept: gzip,deflate',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
            'SOAPAction: ""',
            'Authorization: Basic '. base64_encode(self::USER . ':' . self::PASS),
            'Content-length: ' . strlen($message),
        );
        $hstring = implode("\r\n", $header);
        Log::info("Soap Header: \r\n$hstring");

        // curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 60);
        // curl_setopt($soap_do, CURLOPT_TIMEOUT, 60);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $message);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);

        Log::info("Sending Soap: $soap_do");
        $response = curl_exec($soap_do);
        if($response === false) {
          Log::error('Curl Failed With Errror: ' . curl_error($soap_do));
        } else {
          Log::info('Got a response with length: ' . strlen($response));
        }

        // TODO: Make Sure the response is valid before caching it
        // TODO: Replace default TTL with Response/Provider Specific TTL
        // TODO: Replace Value with Structure to Avoid Hashing collisions
        $errorCode = $this->errorCode( $response );
        if( $errorCode === 0 ) {
            Cache::put($messageHash, $response, self::DEFAULT_TTL);
        }
        return $response;

    }


}
