<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function menu() {
        $url = '/getFlights/';
        return "<a href='$url'>$url</a>";
    }
}
