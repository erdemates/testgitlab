<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('getFlights', 'GalileoController@getFlights');
$router->get('getFlightDetails/{fareInfoRef}/{fareInfoKey}', 'GalileoController@getFlightDetails');
$router->post('bookFlights', 'GalileoController@bookFlights');
$router->get('phpinfo', function(){ return phpinfo();});
$router->get('/', 'Controller@menu');
