<?php /*{
    "segments": [ <?php foreach $segments as $segment { ?>
        {
            "departureAirport":     "<?php $xml->xpath('/segment/departure@airport') ?>",
            "departureTimestamp":   "Unix GMT Timestamp",
            "arrivalAirport":       "IATA 3LC",
            "arrivalTimestamp":     "Unix GMT Timestamp",
            "flightTime":           "minutes",
            "airlineCode":          "IATA 2LC",
            "flightNumber":         "string",
            "baggage": {
                "checked": {
                    "number":       "integer",
                    "weight":       "kg"
                },
                "cabin": {
                    "number":       "integer",
                    "weight":       "kg"
                }
            },
            "class":                "string"
        }
    <?php } ?>
    ],
    "totalFlightTime":              "minutes",
    "nStops":                       "integer",
    "waitTime":                     "minutes",
    "price": {
        "basePrice":                "integer",
        "basePriceCurrency"         "Currency 3LC",
        "tax":                      "integer",
        "taxCurrency":              "Currency 3LC",
        "refundable":               "boolean"
    }
},
