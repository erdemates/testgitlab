<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header/>
    <soapenv:Body>
        <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v42_0" TraceId="trace" TargetBranch="<?=$targetBranch?>" ReturnUpsellFare="true">
            <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="uAPI" />
            <SearchAirLeg>
                <SearchOrigin>
                    <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="<?=$from?>" PreferCity="true" />
                </SearchOrigin>
                <SearchDestination>
                    <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="<?=$to?>" PreferCity="true" />
                </SearchDestination>
                <SearchDepTime PreferredTime="<?=$depDate?>" />
            </SearchAirLeg>
            <?php if( $hasReturn ) { ?>
            <SearchAirLeg>
                <SearchOrigin>
                    <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="<?=$to?>" PreferCity="true" />
                </SearchOrigin>
                <SearchDestination>
                    <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="<?=$from?>" PreferCity="true" />
                </SearchDestination>
                <SearchDepTime PreferredTime="<?=$retDate?>" />
            </SearchAirLeg>
            <?php } ?>
            <AirSearchModifiers>
                <PreferredProviders>
                    <Provider xmlns="http://www.travelport.com/schema/common_v42_0" Code="1G" />
                </PreferredProviders>
            </AirSearchModifiers>
            <?php for ($x = 0; $x < $nAdults; $x++) { ?>
            <SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="ADT" />
            <?php } ?>
            <?php for ($x = 0; $x < $nChildren; $x++) { ?>
            <SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="CHD" />
            <?php } ?>
            <?php for ($x = 0; $x < $nInfants; $x++) { ?>
            <SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="INF" />
            <?php } ?>
            <AirPricingModifiers CurrencyType="<?=$currency?>">
                <AccountCodes>
                    <AccountCode xmlns="http://www.travelport.com/schema/common_v42_0" Code="-" />
                </AccountCodes>
            </AirPricingModifiers>
        </LowFareSearchReq>
</soapenv:Body>
</soapenv:Envelope>
