<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header/>
    <soapenv:Body>
        <air:AirFareRulesReq xmlns:air="http://www.travelport.com/schema/air_v42_0" TargetBranch="<?=$targetBranch?>">
            <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="uAPI" />
            <air:FareRuleKey xmlns:air="http://www.travelport.com/schema/air_v42_0" ProviderCode="<?=$provider?>" FareInfoRef="<?=$fareInfoRef?>"><?=$fareInfoKey?></air:FareRuleKey>
        </air:AirFareRulesReq>
    </soapenv:Body>
</soapenv:Envelope>
