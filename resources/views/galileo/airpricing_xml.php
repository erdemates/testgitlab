<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<soapenv:Header/>
<soapenv:Body>
<AirPriceReq xmlns="http://www.travelport.com/schema/air_v42_0" TraceId="trace" TargetBranch="<?=$targetBranch?>">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="uAPI" />
  <AirItinerary>
    <?php foreach( $departureXmlString->segments as $segment) {?>
    <AirSegment ProviderCode="1G" <?php foreach ($segment as $key => $value) { ?><?=$key?>="<?=$value?>" <?php } ?>/>
    <?php } ?>
    <?php foreach( $returnXmlString->segments as $segment) {?>
    <AirSegment ProviderCode="1G" <?php foreach ($segment as $key => $value) { ?><?=$key?>="<?=$value?>" <?php } ?>/>
    <?php } ?>

  </AirItinerary>
  <AirPricingModifiers InventoryRequestType="DirectAccess">
    <BrandModifiers ModifierType="FareFamilyDisplay" />
  </AirPricingModifiers>
  <SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="ADT" BookingTravelerRef="ek9jN1dDdHVxNHhvN2RyRw==" Key="ek9jN1dDdHVxNHhvN2RyRw==" />
  <AirPricingCommand>
    <?php foreach( $departureXmlString->bookingCodes as $bookingCode) {?>
      <AirSegmentPricingModifiers AirSegmentRef="<?=$bookingCode->segmentRef?>" FareBasisCode="<?=$bookingCode->fareBasis?>">
        <PermittedBookingCodes>
          <BookingCode Code="<?=$bookingCode->bookingCode?>" />
          </PermittedBookingCodes>
        </AirSegmentPricingModifiers>
    <?php } ?>

    <?php foreach( $returnXmlString->bookingCodes as $bookingCode) {?>
      <AirSegmentPricingModifiers AirSegmentRef="<?=$bookingCode->segmentRef?>" FareBasisCode="<?=$bookingCode->fareBasis?>">
        <PermittedBookingCodes>
          <BookingCode Code="<?=$bookingCode->bookingCode?>" />
          </PermittedBookingCodes>
        </AirSegmentPricingModifiers>
    <?php } ?>

      </AirPricingCommand>
      <FormOfPayment xmlns="http://www.travelport.com/schema/common_v42_0" Type="Credit" />
    </AirPriceReq>
  </soapenv:Body>
</soapenv:Envelope>
