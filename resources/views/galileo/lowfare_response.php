<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<?php foreach ($options as  $option) { ?>
<p style="color:#faa800;font-size:24px;display:inline;">
    Ucus #<?=$option['counter']?>
    (Uçuş süresi <?=$option['travelday']?>
    Gün <?=$option['travelhour']?>
    Saat <?=$option['travelmin']?> Dakika)
</p><br>

<table>
    <tr>
        <th>Leg Number</th>
        <th>Flight Number</th>
        <th>Cabin Class</th>
        <th>Origin</th>
        <th>Destination</th>
        <th>BookingCount</th>
        <th>Carrier</th>
        <th>Total Price</th>
        <th>Segment Key</th>
        <th>Duraklama</th>
    </tr>
    <?php foreach($option['flights'] as $flight) {?>
    <tr>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['legNumber']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['flightNumber']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['cabinClass']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['origin']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['destination']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['bookingCount']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['carrier']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['totalPrice']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['segmentKey']?></p></td>
        <td><p style="color:#9e2020;font-size:20px;display:inline;"><?=$flight['nStops']?></p></td>
    </tr>
    <?php } ?>
</table>
<?php } ?>
